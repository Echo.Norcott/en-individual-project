/*  Echo Norcott
    CS205
    Individual Assignment
 */

public class main {
    public static void main(String args[]) {
        Route route = new Route();

        // add some streets to route
        route.addStreetCorrect(new Street("Main St", 2640, 20));
        route.addStreetCorrect(new Street("Church St", 1580, 10));
        route.addStreetCorrect(new Street("Pearl St", 1850, 13));
        route.addStreetCorrect(new Street("College St", 1580, 10));

        // add some doggies
        route.addDogToRoute(new Dog("Perrito", "Chihuahua", 1));
        route.addDogToRoute(new Dog("Chiot", "Papillon", 5));
        route.addDogToRoute(new Dog("Hund", "German Sheppard", 7));
        route.addDogToRoute(new Dog("Puppy", "Boston Terrier", 3));

        // add some walkers
        route.addWalker(new DogWalker("Jules"));
        route.addWalker(new DogWalker("Noah"));

        // assign some dogs to the walkers
        route.assignDog(route.getDogs().get(0), route.getWalkers().get(0));
        route.assignDog(route.getDogs().get(1), route.getWalkers().get(0));
        route.assignDog(route.getDogs().get(2), route.getWalkers().get(1));
        route.assignDog(route.getDogs().get(3), route.getWalkers().get(1));

        // print todays route stats
        // what are the streets we're walking on
        // the distance and how long will it take
        // who's walking who today
        route.printRouteStats();
        System.out.println("");

        // now remove some dogs and reprint
        // oh no ! one puppy ate too much grass and had a belly ache ):
        // remove dog 3 from the route
        System.out.println("Oh no! " + route.getDogs().get(3).getName() +
                " ate too much grass and now has a belly ache ):");
        System.out.println(route.getDogs().get(3).getName() + " will not be walking today");
        route.removeDogFromRoute(route.getDogs().get(3));
        System.out.println("\nUPDATE 1: ");
        route.printRouteStats();
        System.out.println("");

        // we lost a dog !!!!
        // remove dog 2 from the route
        Dog lostDog = route.getDogs().get(2);
        System.out.println("Oh no! We lost " + lostDog.getName());
        route.removeDogFromRoute(lostDog);
        System.out.println("UPDATE 2: ");
        route.printRouteStats();
        System.out.println("");
        
        // oh no church st is closed
        Street closedStreet = route.getStreets().get(1);
        System.out.println("Uh oh, " + closedStreet.getStreetName() + 
            " is closed for a parade");
        route.removeStreetCorrect(closedStreet);
        System.out.println("\nUPDATE 3: ");
        route.printRouteStats();
        System.out.println("");

        // oh phew we found him
        System.out.println("Thank goodness, we found " + lostDog.getName());
        route.addDogToRoute(lostDog);
        route.assignDog(lostDog, route.getWalkers().get(1));
        

        // print todays stats again
        System.out.println("UPDATE 4: ");
        route.printRouteStats();
    }
}