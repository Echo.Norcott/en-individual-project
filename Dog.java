/*  Echo Norcott
    CS201
    Individual Project
 */

// Class to represent a dog ! V● ᴥ ●V
class Dog {
    private String name;
    private String breedType;
    private int age;

    // constuctors
    public Dog(String name, String breedType, int age) {
        this.name = name;
        this.breedType = breedType;
        this.age = age;
    }

    // getters
    public String getName() {
        return this.name;
    }

    public String getBreed() {
        return this.breedType;
    }

    public int getAge() {
        return this.age;
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

}