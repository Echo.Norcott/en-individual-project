/*  Echo Norcott
    CS205
    Individual Assignment
 */

// class to represent a street
class Street {
    private String streetName;
    private double distance;    // in feet please
    private double time;    // in minutes please

    // constructor
    public Street(String streetName, double distance, double time) {
        this.streetName = streetName;
        this.distance = distance;
        this.time = time;
    }

    // getters
    public String getStreetName() {
        return streetName;
    }

    public double getDistance() {
        return distance;
    }

    public double getTime() {
        return time;
    }

    // setters
    public void setStreetName(String newName) {
        streetName = newName;
    }

    public void setDistance(double newDistance) {
        distance = newDistance;
    }

    public void setTime(double newTime) {
        time = newTime;
    }
}
