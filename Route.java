/*  Echo Norcott
    CS205
    Individual Project
 */

import java.util.ArrayList;

// Class to represent a route
// each route has an arraylist of streets, dogs, and walkers
class Route {
    private ArrayList<Dog> dogs;
    private ArrayList<DogWalker> walkers;
    private ArrayList<Street> streets;
    private int totalTime;


    // constructors
    public Route() {
        this.dogs = new ArrayList<Dog>();
        this.walkers = new ArrayList<DogWalker>();
        this.streets = new ArrayList<Street>();
        this.totalTime = 0;
    }

    public Route(ArrayList<Dog> dogs, ArrayList<DogWalker> walkers, ArrayList<Street> streets) {
        this.dogs = dogs;
        this.walkers = walkers;
        this.streets = streets;
        setTotalTimeCorrect();
    }

    // getters
    public ArrayList<Dog> getDogs() {
        return dogs;
    }

    public ArrayList<DogWalker> getWalkers() {
        return walkers;
    }

    public ArrayList<Street> getStreets() {
        return streets;
    }

    public int getTotalTime() {
        return totalTime;
    }

    // setters
    public void setDogs(ArrayList<Dog> dogs) {
        this.dogs = dogs;
    }

    public void setWalkers(ArrayList<DogWalker> walkers) {
        this.walkers = walkers;
    }

    public void setStreets(ArrayList<Street> streets) {
        this.streets = streets;
    }

    // for setting the total time of the route
    // we want to take the time it takes to walk each street
    // by going through the arraylist and adding each time to the total
    public void setTotalTimeCorrect() {
        int total = 0;
        for (int i = 0; i < streets.size(); i++) {
            total += streets.get(i).getTime();
        }
        this.totalTime = total;
    }

    // now let's do it wrong!
    public void setTotalTimeWrong() {
        for (int i = 0; i < streets.size(); i++) {
            this.totalTime += streets.get(i).getTime();
        }
    }

    // add to functions for each arraylist
    public void addDogToRoute(Dog newDog) {
        dogs.add(newDog);
    }

    // for adding walkers, we want to add
    // each of their dogs to the dog array as well
    // if the dog is not already in the array
    public void addWalker(DogWalker newWalker) {
        walkers.add(newWalker);
        for (int i = 0; i < newWalker.getDogs().size(); i++) {
            if (!findDogInRoute(newWalker.getDogs().get(i))) {
                dogs.add(newWalker.getDogs().get(i));
            }
        }
    }

    public void addStreetCorrect(Street newStreet) {
        streets.add(newStreet);
        // we want to update the total time as well
        setTotalTimeCorrect();
    }

    public void addStreetWrong(Street newStreet) {
        streets.add(newStreet);
        setTotalTimeWrong();
    }

    // delete from functions for each arraylist
    public void removeDogFromRoute(Dog byeDog) {
        for (int i = 0; i < dogs.size(); i++) {
            if (dogs.get(i) == byeDog) {
                dogs.remove(byeDog);
            }
        }
        // we also want to delete the dog from the walker
        for (int j = 0; j < walkers.size(); j++) {
            if (walkers.get(j).findDog(byeDog)) {
                walkers.get(j).removeDog(byeDog);
            }
        }
    }

    public void removeWalker(DogWalker byeWalker) {
        // we want to find the walker
        for (int i = 0; i < walkers.size(); i++) {
            if (walkers.get(i) == byeWalker) {
                walkers.remove(byeWalker);
            }
        }
    }

    public void removeStreetCorrect(Street byeStreet) {
        for (int i = 0; i < streets.size(); i++) {
            if (streets.get(i) == byeStreet) {
                // we want to make sure that the time of the street
                // is removed from the total time as well
                streets.remove(byeStreet);
            }
        }
        setTotalTimeCorrect();
    }

    public void removeStreetWrong(Street byeStreet) {
        for (int i = 0; i < streets.size(); i++) {
            if (streets.get(i) == byeStreet) {
                // we want to make sure that the time of the street
                // is removed from the total time as well
                streets.remove(byeStreet);
            }
        }
        setTotalTimeWrong();
    }

    // finding items
    public boolean findDogInRoute(Dog whichDog) {
        for (int i = 0; i < this.dogs.size(); i++) {
            if (this.dogs.get(i) == whichDog) {
                return true;
            }
        }
        return false;
    }

    // assigning a dog to a walker
    public void assignDog(Dog newDog, DogWalker toWalker) {
        // first we want to check to see if the dog is already assigned to a dogwalker
        // if the dog is, we want to remove it from that walker
        for (int i = 0; i < this.walkers.size(); i++) {
            if (this.walkers.get(i).findDog(newDog)) {
                if (this.walkers.get(i) != toWalker) {
                    this.walkers.get(i).removeDog(newDog);
                }
            }
        }
        // then we can add the dog to the walker
        toWalker.addDog(newDog);
    }

    // print the stats of the route
    public void printRouteStats() {
        int distance = 0;
        System.out.println("TODAY'S ROUTE");
        System.out.println("Streets: ");
        for (int i = 0; i < streets.size(); i++) {
            System.out.println("* " + streets.get(i).getStreetName() + " for "
                + streets.get(i).getDistance() + " feet");
            distance += streets.get(i).getDistance();
        }
        System.out.println("The total distance for todays route will be " + distance +
                " feet and will take approximately " + totalTime + " minutes");
        System.out.println("Assignments:");
        for (int j = 0; j < walkers.size(); j++) {
            System.out.println("* " + walkers.get(j).getName() + " will be walking:");
            for (int k = 0; k < walkers.get(j).getDogs().size(); k++) {
                Dog currentDog = walkers.get(j).getDogs().get(k);
                System.out.println("    - " + currentDog.getName() +
                        " a " + currentDog.getAge() + " year old " + currentDog.getBreed());
            }
        }
    }

}