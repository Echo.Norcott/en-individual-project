/*  Echo Norcott
    CS205
    Individual Assignment
 */

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import java.util.ArrayList;

public class RouteTest {
    private static Route route;
    private static Street main;
    private static Street church;
    private static DogWalker jules;
    private static DogWalker noah;
    private static Dog perrito;
    private static Dog chien;

    @BeforeAll
    public static void setUp() {
        route = new Route();
        main = new Street("Main St", 2640, 20);
        church = new Street("Church St", 1580, 10);
        jules = new DogWalker("Jules");
        noah = new DogWalker("Noah");
        perro = new Dog("Perrito", "Chihuahua", 1);
        chien = new Dog("Chiot", "Papillon", 5);
    }

    // * * * * * TEST #1 * * * * *
    // In this test I want to test assigning a dog to a dog walker
    // And then trying to assign that same dog to another walker
    // To check that it removes the dog from the original dog walker
    // And adds it to the other
    @Test
    public void testAssignDogOne() {
        route.addWalker(jules);
        route.addWalker(noah);

        route.addDogToRoute(perro);

        // Size of dogs assigned to either walker should be 0 originally
        Assert.assertEquals(jules.getDogs().size(), 0);
        Assert.assertEquals(noah.getDogs().size(), 0);

        // Assign perro to Jules
        route.assignDog(perro, jules);
        Assert.assertEquals(jules.getDogs().size(), 1);
        Assert.assertEquals(noah.getDogs().size(), 0);

        // Check to see that perro is in jules's dogs list
        Assert.assertTrue(jules.findDog(perro));
        // And not in noah's
        Assert.assertFalse(noah.findDog(perro));

        // Assign perro to Noah
        route.assignDog(perro, noah);
        Assert.assertEquals(jules.getDogs().size(), 0);
        Assert.assertEquals(noah.getDogs().size(), 1);

        // Check to see that perro is not in jules's dogs list
        Assert.assertFalse(jules.findDog(perro));
        // And not in noah's
        Assert.assertTrue(noah.findDog(perro));
    }
    // * * * * * END TEST #1 * * * * *

    // * * * * * TEST #2 * * * * *
    // in this test I want to assign a dog to a walker
    // who already has this dog in their list
    // to make sure that the dog is not added twice to the list
    @Test
    public void testAssignDogTwo() {
        route.addWalker(jules);
        route.addDogToRoute(perro);

        // number of dogs assigned to jules should be 0 originally
        Assert.assertEquals(jules.getDogs().size(), 0);

        // assign perro to jules's list
        route.assignDog(perro, jules);

        // check that the number of dogs is 1
        Assert.assertEquals(jules.getDogs().size(), 1);

        // try to assign perro to jules again
        route.assignDog(perro, jules);

        // make sure that jules's number of dogs is still only 1
        Assert.assertEquals(jules.getDogs().size(), 1);
    }
    // * * * * * END TEST #2 * * * * *

    // * * * * * TEST #3 * * * * *
    // in this test I want to test that removing a dog from the route
    // also deletes the dog from the walker that had the dog in their list
    @Test
    public void testRemoveDogFromRoute() {
        route.addWalker(jules);
        route.addDogToRoute(perro);
        route.addDogToRoute(chien);

        // number of dogs assigned to jules should originally be 0
        Assert.assertEquals(jules.getDogs().size(), 0);
        // number of dogs in route should originally be 2
        Assert.assertEquals(route.getDogs().size(), 2);

        // assign dogs to jules
        route.assignDog(perro, jules);
        route.assignDog(chien, jules);
        // check that jules has two dogs in list
        Assert.assertEquals(jules.getDogs().size(), 2);

        // remove dog from route
        route.removeDogCorrect(perro);
        // make sure dog was removed from route
        Assert.assertFalse(route.findDogInRoute(perro));
        Assert.assertEquals(route.getDogs().size(), 1);

        // make sure that dog is not in jules's list
        Assert.assertFalse(jules.findDog(perro));
        Assert.assertEquals(jules.getDogs().size(), 1);
    }
    // * * * * * END TEST #3 * * * * *

    // * * * * * TEST #4 * * * * *
    // in this test I want to test setTotalTime
    // when adding and removing streets using the INCORRECT setTotalTime function
    // I had to make an incorrect functions for addStreet and removeStreet
    // as these functions call setTotalTime to update with each addition / removal
    @Test
    public void testSetTotalTimeIncorrect() {
        route.addStreetWrong(main); // main st time = 20
        Assert.assertEqual(route.getTime(), main.getTime());
        route.addStreetWrong(church); // church st time = 10

        // total time should equal church and main together = 30
        // when implemented wrong, totalTime will equal 50
        Assert.assertEqual(route.getTime(), main.getTime() + church.getTime());

        route.removeStreetWrong(main);
        // removing main st should make totalTime = 10
        // with the incorrect implementation will make it = 60
        Assert.assertEqual(route.getTime(), church.getTime());

        route.removeStreetWrong(church);
        // removing all streets should make totalTime = 0
        // with incorrect implementation it will = 60
        Assert.assertEqual(route.getTime(), 0);
    }
    // * * * * * END TEST #4 * * * * *

    // * * * * * TEST #5 * * * * *
    // in this test I want to test setTotalTime
    // when adding and removing streets using the CORRECT setTotalTime function
    // this function uses the correct addStreet and removeStreet
    // the correct implementation resets the totalTime to 0
    // before going through each street in the street list
    // and adding their time to totalTime
    @Test
    public void testSetTotalTimeCorrect() {
        route.addStreetCorrect(main); // main st time = 20
        Assert.assertEqual(route.getTime(), main.getTime());
        route.addStreetCorrect(church); // church st time = 10

        // total time should equal church and main together = 30
        Assert.assertEqual(route.getTime(), main.getTime() + church.getTime());

        route.removeStreetCorrect(main);
        // removing main st should make totalTime = 10
        Assert.assertEqual(route.getTime(), church.getTime());

        route.removeStreetCorrect(church);
        // removing all streets should make totalTime = 0
        Assert.assertEqual(route.getTime(), 0);
    }
    // * * * * * END TEST #5 * * * * *
}