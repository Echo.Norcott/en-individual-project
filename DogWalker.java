/*  Echo Norcott
    CS205
    Individual Assignment
 */

// class to represent a dog walker !

import java.util.ArrayList;

class DogWalker {
    private String name;
    private ArrayList<Dog> dogs;

    // constructors
    public DogWalker() {
        this.name = "jane doe";
        this.dogs = new ArrayList<Dog>();
    }

    public DogWalker(String name) {
        this.name = name;
        this.dogs = new ArrayList<Dog>();
    }

    public DogWalker(String name, ArrayList<Dog> dogs) {
        this.name = name;
        this.dogs = dogs;
    }

    // getters
    public String getName() {
        return name;
    }

    public ArrayList<Dog> getDogs() {
        return dogs;
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }

    public void setDogs(ArrayList<Dog> dogs) {
        this.dogs = dogs;
    }

    // add dogs
    public void addDog(Dog newDog) {
        dogs.add(newDog);
    }

    // remove dog ):
    public void removeDog(Dog byeDog) {
        dogs.remove(byeDog);
    }

    // return boolean of whether or not dog is in the list
    public boolean findDog(Dog whichDog) {
        for (int i = 0; i < dogs.size(); i++) {
            if (dogs.get(i) == whichDog) {
                return true;
            }
        }
        return false;
    }
}